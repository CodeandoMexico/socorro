function createMap(latitude, longitude, zoom) {
	let map = L.map('map').setView([latitude, longitude], zoom);

	L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
	}).addTo(map);

	return map;
}

function deleteMap() {
	let map = document.getElementById('map');
	if(map) {
		map.parentElement.remove();
	}
}

async function fetchMapInfo(url, state) {
	if (state == "Todos los estados") {
		state = '';
	}
	let allData = await fetchData(url);
	let docs = []

	for (const data of allData.records) {
		let stateFetched = data.fields.nombre_estado
		if (stateFetched && (stateFetched.normalized() == state.normalized() || !state)) {
			let fields = data.fields;
			docs.push(fields);
		}
	}
	
	if(!docs) throw new Error("Estado no coincide con la base de datos");

	return docs;
}

async function fetchDocs(url, state) {
	let data = await fetchData(url);
	let docs = [];
	for(const record of data.records) {
		let fields = record.fields;
		if(fields.nombre_estado == state || state == "Todos los estados") {
			docs.push(fields);
		}
	}

	return docs;
}

function generateCustomMarker() {
	let html = `<svg width="45" height="60" viewBox="0 0 45 60" fill="none" xmlns="http://www.w3.org/2000/svg">
	<g style="mix-blend-mode:multiply" filter="url(#filter0_d_0_1)">
	<path d="M41 20.065C41 31.1467 17 68 23 42.5C30.5 68 4 31.1467 4 20.065C4 8.98343 12.2827 0 22.5 0C32.7173 0 41 8.98343 41 20.065Z" fill="#110E38"/>
	<path d="M23.2398 42.4295L22.9731 41.5227L22.7566 42.4427C22.0055 45.635 21.717 47.8774 21.8012 49.3263C21.831 49.8388 21.9081 50.2675 22.0374 50.6088C21.5689 50.3166 21.0369 49.8907 20.4549 49.3468C19.2823 48.2507 17.9284 46.6956 16.5108 44.8484C13.6765 41.1552 10.6064 36.3193 8.24588 31.7174C7.06571 29.4166 6.06499 27.1783 5.35986 25.1736C4.65316 23.1645 4.25 21.4081 4.25 20.065C4.25 9.10199 12.4395 0.25 22.5 0.25C32.5605 0.25 40.75 9.10199 40.75 20.065C40.75 21.412 40.384 23.1711 39.7423 25.1805C39.1019 27.186 38.1923 29.4248 37.1173 31.7257C34.9669 36.328 32.1618 41.1636 29.546 44.8561C28.2376 46.703 26.9813 48.2573 25.8822 49.3523C25.3477 49.8848 24.8556 50.3033 24.4173 50.5938C24.5299 50.2511 24.5844 49.8227 24.5869 49.313C24.5939 47.8637 24.1786 45.6213 23.2398 42.4295ZM23.8171 50.7199C23.6757 50.9561 23.4886 51.0592 23.2389 51.0612C22.9944 51.0593 22.7997 50.9564 22.6434 50.7132C22.4682 50.4405 22.3404 49.9863 22.3004 49.2973C22.2287 48.0649 22.442 46.1796 23.0277 43.5055C23.7726 46.1871 24.0929 48.0766 24.0869 49.3106C24.0836 49.9999 23.9781 50.4511 23.8171 50.7199Z" stroke="#110E38" stroke-width="0.5"/>
	</g>
	<g>
	<path d="M15 11H30V33H15V11Z" fill="#F09B5C"/>
	</g>
	<path d="M21.6765 28.5L23.3235 28.5V32.5H21.6765V28.5Z" fill="white" stroke="white"/>
	<rect x="18.5294" y="14" width="2.64706" height="3" fill="white"/>
	<rect x="23.8235" y="18" width="2.64706" height="3" fill="white"/>
	<rect x="23.8235" y="22" width="2.64706" height="3" fill="white"/>
	<rect x="18.5294" y="22" width="2.64706" height="3" fill="white"/>
	<rect x="18.5294" y="18" width="2.64706" height="3" fill="white"/>
	<rect x="23.8235" y="14" width="2.64706" height="3" fill="white"/>
	<defs>
	<filter id="filter0_d_0_1" x="0" y="0" width="45" height="59.3112" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
	<feFlood flood-opacity="0" result="BackgroundImageFix"/>
	<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
	<feOffset dy="4"/>
	<feGaussianBlur stdDeviation="2"/>
	<feComposite in2="hardAlpha" operator="out"/>
	<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
	<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_0_1"/>
	<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_0_1" result="shape"/>
	</filter>
	</defs>
	</svg>
	`;
	return L.divIcon({
		html: html,
		className: "",
		iconSize: [12, 12],
		iconAnchor: [12, 12],
	  });
}

function text_formated(doc, entityName, entityTableName, color) {
	let instituteName   = doc[entityTableName],
	phoneNumber     = doc[`telefono_1`],
	address         = doc[`direccion`],
	state           = doc[`nombre_estado`];
	if (!doc['texto_para_bot']) {
		return `
		<b>${entityName}</b> - <b>${state}</b><br>
		<b>${instituteName}</b><br>
		<b>Teléfono(s):</b> ${phoneNumber}<br>
		<b>Dirección:</b> ${address}<br>
		`
	} else {
		return `
			<p>${
				doc['texto_para_bot']
				.replace('📍', '</p><p>📍')
				.replace('📞', '</p><p>📞')
				.replace('📧', '</p><p>📧')
				.replace('https', '</p><p>https')
			}
		` + '</p>';
	}
}

function createMarkers(map, docs, currentInclude) {
	let markers = [];
	let coords = []
	for (const [index, doc] of docs.entries()) {
		let properties      = currentInclude.mapProperties,
				entityTableName = properties.entityTableName,
				entityName      = properties.entityName,
				latitude        = doc[`latitud`],
				longitude       = doc[`longitud`],
				color           = properties.color
		
		if(latitude && longitude) {
			coords.push([latitude, longitude])
			let marker = L.marker([latitude, longitude], {
				icon: generateCustomMarker()
			}).bindPopup(text_formated(doc, entityName, entityTableName, color));
			markers.push(marker);
		}
	}

	return [coords, markers];
}


async function MapGenerator(includes) {
	let state = includes[0].state;
	let data = await fetchMapInfo(
		url = "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Estados?sort%5B0%5D%5Bfield%5D=num_estado&sort%5B0%5D%5Bdirection%5D=asc",
		estado = state
	);

	let map = createMap(23.70866248924945, -102.62600610469572, 5.0);
	let markerArray = []
	let globalCoords = []
	for(const [index, include] of includes.entries()) {
		let docs = await fetchDocs(
			url = include.url,
			state = state
		);

		let [tempCoords, tempMarkers] = createMarkers(
			map = map,
			docs = docs,
			currentInclude = include
		)
		
		globalCoords.push(...tempCoords)
		markerArray.push(tempMarkers);
	}

	let layerGroups = [];

	for (const markers of markerArray) {
		layerGroups.push(L.layerGroup(markers));
	}

	let overlayMaps = {}
	for (const [index, include] of includes.entries()) {
		overlayMaps[include.title] = layerGroups[index];
		layerGroups[index].addTo(map);
	}
	L.geoJson(
		mexicoJson,
		{
			style: {
				color: '#9FB4FF',
				weight: 2,
				fill: false,
			}
		}
	).addTo(map);

	L.control.layers(null, overlayMaps).addTo(map);

	if (
		includes[0].state != "Todos los estados" &&
		includes[0].state != "Todos los paises"
	) {
		map.fitBounds(globalCoords)
		map.zoomOut()
	}


}