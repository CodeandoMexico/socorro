async function preGeneration(includes) {
	deleteAllPage();
	deleteTables();
	deleteMap();
	generatePage(includes);
}

function deleteAllPage() {
	const allSections = document.querySelectorAll('section') //c-seleccion-estado__section
	allSections.forEach((section) => {
		if (section.id != 'c-seleccion-estado__section') {
			section.parentNode.removeChild(section);
		}
	}) 
}

async function generatePage(includes) {
	const section = document.getElementById("c-seleccion-estado__section");
	
	// Generate map
	if (includes[0].mapProperties) {
		// Map Header

		// Creating map
		let mapContainer = document.createElement("section");
		mapContainer.classList.add("c-map__container", 'container', 'animate');
		let mapDiv = document.createElement("div");
		mapDiv.id = "map";

		mapContainer.appendChild(mapDiv);
		section.appendChild(mapContainer);

		MapGenerator(includes);
	}
	


	// Generate table or tables
	let tables = [];
	// Generate tables
	for(let include of includes) {
		const c = "c-table responsive";
		const container = document.createElement("section");
		addMobileColumns(include.columnNames, include.tableName);
		container.classList.add("container", "table__container");
		container.innerHTML = `<div class="${c}">
		<h1 class="table__title">${include.title}</h1>
		<div id="${include.tableName}-table" class="${c}"></div>
		</div>`;
		section.appendChild(container);		
		// Table generation
		let table = await createTables(include.url, include.columnNames, include.title, include.state, include.place);
		tables.push({
			tableName: include.tableName,
			table: table
		});
	}

	tables.forEach(table => {
		if (table.table.querySelectorAll('tr').length > 1) {
			document.getElementById(`${table.tableName}-table`)
			.appendChild(table.table);
		} else {
			document.getElementById(`${table.tableName}-table`).innerHTML = `<p>En el ${includes[0].entity} que seleccionaste no he encontrado ningún colectivo, si tú conoces alguno por favor ponte en contacto con nuestro equipo.</p>`
		}
	});

}

function selectorFunction(e, state) {
	if (e.target.value && e.target.value != state) {
		searchBtn.style.opacity = "1";
		searchBtn.style.pointerEvents = "auto";
	}
	else {
		searchBtn.style.opacity = "0.5";
		searchBtn.style.pointerEvents = "none";
	}
}

