// ================== GENERATE PAGE ================== //
const searchBtn = document.getElementById("search-btn");
const selector = document.getElementById("colectivos__selector");

const params = new URLSearchParams(window.location.search);
let state = params.get('estado');

selector.addEventListener("change", e => {
	selectorFunction(e, state);
	state = e.target.value;
});

includes = [
	{
		entity: 'estado',
		// El primero es como se llama en airtable y el segundo como se desplegará
		columnNames: [
			["colectivo", "Colectivo"],
			["nombre_estado", "Estado"],
			["url_fuente", "Información", 'url_fuente'],
			["REDES", "Redes sociales"],
		],
		url: "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Colectivos?sort%5B0%5D%5Bfield%5D=num_estado",
		state: state,
		tableName: "colectivos",
		title: "Colectivos",
		place: 'nombre_estado'
	},
];

if (state) {
	const selector = document.querySelector('select');
	selector.value = state
	preGeneration(includes);	
} else {
	searchBtn.style.opacity = "0.5";
	searchBtn.style.pointerEvents = "none";
}

searchBtn.addEventListener("click", async function(e) {
	this.style.opacity = "0.5";
	this.style.pointerEvents = "none";
	window.location.href = window.location.origin +
		window.location.pathname + `?estado=${state}`;
});