// ================== GENERATE PAGE ================== //
const searchBtn = document.getElementById("search-btn");
const selector = document.getElementById("desde-el-extranjero__selector");

const params = new URLSearchParams(window.location.search);
let state = params.get('pais');

selector.addEventListener("change", e => {
	selectorFunction(e, state);
	state = e.target.value;
});

includes = [
	{
		entity: 'país',
		// El primero es como se llama en airtable y el segundo como se desplegará
		columnNames: [
			["ciudad", "Autoridad"],
			["pais", "País"],
			["direccion", "Dirección", 'url_google_maps'],
			["telefono", "Teléfono"], // Add more phones
			["correo_electronico", "Correo"], // Add more emails
		],
		url: "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Embajadas-Consulados?sort%5B0%5D%5Bfield%5D=id",
		state: state,
		tableName: "embajadasConsulados",
		title: "Embajadas y Consulados",
		place: 'pais'
	},

	{
		// El primero es como se llama en airtable y el segundo como se desplegará
		columnNames: [
			["unidad_administrativa", "Autoridad"],
			["contacto", "Contacto"],
			["direccion", "Dirección", 'url_google_maps'],
			["telefono", "Teléfono"],
			["correo_electronico", "Correo"],
		],
		url: "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Agregadurias-FGR?sort%5B0%5D%5Bfield%5D=id",
		state: "SHOW",
		tableName: "agregaduriasFgr",
		title: "Agregadurías de la FGR",
		place: 'pais'
	},

	{
		// El primero es como se llama en airtable y el segundo como se desplegará
		columnNames: [
			["nombre", "Autoridad"],
			["pagina_web", "Página web", 'pagina_web'],
			["direccion", "Dirección", 'url_google_maps'],
			["telefono_*", "Teléfonos"],
			["correo_electronico", "Correo"],
		],
		url: "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Defensorias-Centroamerica?sort%5B0%5D%5Bfield%5D=id",
		state: "SHOW",
		tableName: "defensoriaCentroamerica",
		title: "Defensoria en Centro América",
		place: 'pais'
	},
];

if (state) {
	const selector = document.querySelector('select');
	selector.value = state
	preGeneration(includes);	
} else {
	searchBtn.style.opacity = "0.5";
	searchBtn.style.pointerEvents = "none";
}

searchBtn.addEventListener("click", async function(e) {
	this.style.opacity = "0.5";
	this.style.pointerEvents = "none";
	window.location.href = window.location.origin +
		window.location.pathname + `?pais=${state}`;
});