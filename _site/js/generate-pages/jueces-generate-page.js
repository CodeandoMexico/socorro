// ================== GENERATE PAGE ================== //
const searchBtn = document.getElementById("search-btn");
const selector = document.getElementById("jueces__selector");

const params = new URLSearchParams(window.location.search);
let state = params.get('estado');

selector.addEventListener("change", e => {
	selectorFunction(e, state);
	state = e.target.value;
});

includes = [
	{
		entity: 'estado',
		// El primero es como se llama en airtable y el segundo como se desplegará
		columnNames: [
			["juzgado", "Juzgado"],
			["nombre_estado", "Estado"],
			["nombre", "Nombre"],
			["direccion", "Dirección", 'url_google_maps'],
			["telefono_*", "Teléfonos"], // Add more phones
		],
		url: "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Jueces?sort%5B0%5D%5Bfield%5D=num_estado",
		state: state,
		tableName: "jueces",
		title: "Jueces",
		place: 'nombre_estado',
		mapProperties: {
			color: "#DD4A48",
			entityName: "Juzgado",
			entityTableName: "juzgado"
		},
	},
	{
		entity: 'estado',
		// El primero es como se llama en airtable y el segundo como se desplegará
		columnNames: [
			["area_adscripcion", "Delegación"],
			["nombre_estado", "Estado"],
			["direccion", "Dirección", 'url_google_maps'],
			["telefono_*", "Teléfonos"], // Add more phones
		],
		url: "https://api.airtable.com/v0/appN9DiiAtnz6UOs5/Delegaciones-FGR?sort%5B0%5D%5Bfield%5D=num_estado",
		state: state,
		tableName: "delegacionesFgr",
		title: "Delegaciones FGR",
		place: 'nombre_estado',
		mapProperties: {
			color: "#DD4A48",
			entityName: "Delegación",
			entityTableName: "delegacion"
		},
	},
];

if (state) {
	const selector = document.querySelector('select');
	selector.value = state
	preGeneration(includes);	
} else {
	searchBtn.style.opacity = "0.5";
	searchBtn.style.pointerEvents = "none";
}

searchBtn.addEventListener("click", async function(e) {
	this.style.opacity = "0.5";
	this.style.pointerEvents = "none";
	window.location.href = window.location.origin +
		window.location.pathname + `?estado=${state}`;
});