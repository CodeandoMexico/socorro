async function createTables(url, columnNames, title = "Nombre", state = "", place) {
	let data, fetching = true;
	let records = []
	while (fetching) {
		data = await fetchData(url);
		if (data.offset) {
			url = url.split('&offset')[0];
			url += '&offset=' + data.offset;
		} else {
			fetching = false;
		}
		records.push(...data.records);
	}

	tableComps = createTable(title, columnNames);
	tableComps = createRows(tableComps=tableComps, data=records, state=state, columnNames=columnNames, place=place);
	tableComps.state = state;
	return tableComps.table;

}

function createTable(title, columnNames) {
	let table = document.createElement('table');
	let thead = document.createElement('thead');
	let tbody = document.createElement('tbody');
	
	table.appendChild(thead);
	table.appendChild(tbody);

	let rows = [], headings = [];
	// Create headings
	for(const [index, columnName] of columnNames.entries()) {
		headings.push(document.createElement("th"))
		headings[index].innerHTML = columnName[1]
	}
	rows.push(document.createElement("tr"));
	headings.forEach(heading => {
		rows[0].appendChild(heading);
	});
	thead.appendChild(rows[0]);

	return {
		table: table,
		tbody: tbody,
	};
}

function preprocessData(info, fields) {
	const [nameFetched, nameDisplayed, url] = info;
	const l = nameFetched.length
	let result = fields[info[0]]

	if (nameFetched[l - 1] === '*') {
		let counter = 1;
		result = ''
		let actualField = nameFetched.slice(0, l-1) + counter;
		while (fields[actualField]) {
			result += `<p>${fields[actualField]}</p>`;
			counter += 1
			actualField = nameFetched.slice(0, l - 1) + counter;
			if (!fields[actualField]) result += '<br>'
		}
	}
	if (url) {
		result = `<a target="_blank" href='${fields[url]}'>${fields[nameFetched]}</a>`
	}
	if (nameFetched === 'REDES') {
		if (fields['url_facebook']) {
			result = `
				<a target="_blank" href="${fields['url_facebook']}">Facebook</a>
			`
		}
		if (fields['url_facebook'] && fields['url_twitter']) {
			result += '<br>'
		}
		if (fields['url_twitter']) {
			result += `<a target="_blank" href="${fields['url_twitter']}">Twitter</a>`
		}
	}

	if (!result) {
		result = 'NO DISPONIBLE POR EL MOMENTO'
	}
	
	return result;
}

function createRows(tableComps, data, state, columnNames, place) {
	// Create rows
	for (const record of data) {
		let row = document.createElement("tr");
		let fields = record.fields;
		let state_name = fields[place];
		if (!state_name) {
			state_name = "NONE";
		}
		if (Array.isArray(state_name)) state_name = state_name[0]
		if (state == "Todos los estados" || state_name.normalized() == state.normalized() || state === "SHOW" || state === "Todos los paises")
			for (const [index, info] of columnNames.entries()) {
				let rowData = document.createElement('td');
				rowData.innerHTML = preprocessData(info, fields)
				
				row.appendChild(rowData);
			}
		else continue;
		tableComps.tbody.appendChild(row)
	}

	return tableComps
}


function deleteTables() {
	let tables = [];
	tables = document.querySelectorAll(".c-table");
	if(tables.length) {
		tables.forEach(table => {
			table.parentElement.remove();
		});
	}
}


function createMapsIcon(rowData, fields, info) {
	rowData.innerHTML = `<i class="uil uil-map-pin"></i>`;
	rowData.style.cursor = 'pointer';
	rowData.style.textAlign = 'center';
	rowData.style.fontSize = '2rem';
	rowData.addEventListener('click', e => {
		window.open(fields[info[0]]);
	});
}


function addMobileColumns(columns, tableName) {
	if (columns) {
		const columnNames = columns.map((name) => name[1]);
		const columnLength = columnNames.length; 
		let sheet = document.createElement('style');

		for (let index = 0; index < columnLength; index++) {
			sheet.innerHTML = sheet.innerHTML += `@media
			only screen 
			and (max-width: 760px), (min-device-width: 768px) 
			and (max-device-width: 1024px) { .responsive #${typeTable[tableName]} td:nth-of-type(${index+1}):before { content: '${columnNames[index]}'; font-weight: bold }`;
			document.head.appendChild(sheet);
		}
	}
}

const typeTable = {
	colectivos: 'colectivos-table',
	comisionesEstatal: 'comisionesEstatal-table',
	fiscalias: 'fiscalias-table',
	comisiones: 'comisiones-table',
	agregaduriasFgr: 'agregaduriasFgr-table',
	defensoriaCentroamerica: 'defensoriaCentroamerica-table',
	jueces: 'jueces-table',
	delegacionesFgr: 'delegacionesFgr-table',
	embajadasConsulados: 'embajadasConsulados-table'
}